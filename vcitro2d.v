module vcitro2d

#include <citro2d.h>
struct Pos {
	x f32
	y f32
	w f32
	h f32
}

struct Center {
	x f32
	y f32
}

type RenderTarget = &C.C3D_RenderTarget

[typedef]
struct C.C3D_RenderTarget {}

[typedef]
struct C.C3D_Mtx {}

[typedef]
struct C.C2D_DrawParams {
	pos    Pos
	center Center
	depth  f32
	angle  f32
}

//[typedef]
// struct C.C2D_Tint {
// color u32
// blend f32
//}
[typedef]
struct C.C2D_Image {}

//[typedef]
// struct C.C2D_ImageTint {
////corners [4]C.C2D_Tint
//}
fn C.C2D_Clamp(x f32, min f32, max f32) f32

fn C.C2D_FloatToU8(f32) byte

fn C.C2D_Color32(r byte, g byte, b byte, a byte) u32

fn C.C2D_Color32f(r f32, g f32, b f32, a f32) u32

fn C.C2D_Init(max_objects int) bool

fn C.C2D_Fini()

fn C.C2D_Prepare()

fn C.C2D_Flush()

fn C.SceneSize(width u32, height u32, tilt bool)

fn C.C2D_SceneTarget(&C.C3D_RenderTarget)

fn C.C2D_ViewReset()

fn C.C2D_ViewSave(&C.C3D_Mtx)

fn C.C2D_ViewRestore(&C.C3D_Mtx)

fn C.C2D_ViewTranslate(f32, f32)

fn C.C2D_ViewRotate(f32)

fn C.C2D_ViewRotateDegrees(f32)

fn C.C2D_ViewShear(f32, f32)

fn C.C2D_ViewScale(f32, f32)

fn C.C2D_CreateScreenTarget(screen int, side int) &C.C3D_RenderTarget

fn C.C2D_TargetClear(&C.C3D_RenderTarget, u32)

fn C.C2D_SceneBegin(&C.C3D_RenderTarget)

fn C.C2D_Fade(u32)

// fn C.C2D_DrawImage(C.C2D_Image, &C.C2D_DrawParams, &C.C2D_ImageTint)
fn C.C2D_DrawRectangle(f32, f32, f32, f32, f32, u32, u32, u32, u32)

fn C.C2D_DrawRectSolid(f32, f32, f32, f32, f32, u32)

enum Corner {
	top_left
	top_right
	bottom_left
	bottom_right
}

pub fn init(max_objects int) {
	C.C2D_Init(max_objects)
}

pub fn fini() {
	C.C2D_Fini()
}

pub fn prepare() {
	C.C2D_Prepare()
}

pub fn flush() {
	C.C2D_Flush()
}

pub fn create_screen_target(screen int, side int) &C.C3D_RenderTarget {
	target := C.C2D_CreateScreenTarget(screen, side)
	return target
}

pub fn color32(r byte, g byte, b byte, a byte) u32 {
	return C.C2D_Color32(r, g, b, a)
}

pub fn (target &C.C3D_RenderTarget) clear(color u32) {
	C.C2D_TargetClear(target, color)
}

pub fn (target &C.C3D_RenderTarget) begin() {
	C.C2D_SceneBegin(target)
}

pub fn draw_rect_solid(x f32, y f32, z f32, w f32, h f32, clr u32) {
	C.C2D_DrawRectSolid(x, y, z, w, h, clr)
}
